import 'ol/ol.css';
import OSM from 'ol/source/OSM';
import KML from 'ol/format/KML';
import {Map, View} from 'ol';
import {Tile, Vector} from 'ol/layer';
import VectorSource from 'ol/source/Vector';
import Style from 'ol/style/Style';
import Fill from 'ol/style/Fill';
import Stroke from 'ol/style/Stroke';

/* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** ***
 * CANDIDATES
 */

const IMAGE_URL_PREFIX = 'https://d3n8a8pro7vhmx.cloudfront.net/futureparty/pages';
const LINK_URL_PREFIX = 'https://www.scienceparty.org.au';
const JSON_PATH = './data/candidates.json';

// See note in the readme.
const KML_PATH = null;

let fs = require('fs');
let data_raw = fs.readFileSync(JSON_PATH, 'utf8');

const DATA = JSON.parse(data_raw);

function hasCandidate (division_name) {
    return division_name in DATA;
}

function getCandidateName (division_name) {
    return DATA[division_name]['name'];
}

function getImageUrl (division_name) {
    return IMAGE_URL_PREFIX + DATA[division_name]['image'];
}

function getLinkUrl (division_name) {
    return LINK_URL_PREFIX + DATA[division_name]['link'];
}

/* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** ***
 * STYLES
 */

const STYLE_NORMAL = 0;
const STYLE_HIGHLIGHT = 1;
const STYLE_SELECT = 2;

const ALPHA = 0.4;
const STROKE_COLOR = 'rgba(0, 0, 0, 1)';
const STROKE_WIDTH = 1.25;

const STYLES = {
    false: [
        createStyle (192, 192, 192),
        createStyle (255, 255, 255),
        createStyle (128, 128, 128)
    ],
    true: [
        createStyle (  0, 128, 255),
        createStyle ( 64, 160, 255),
        createStyle (  0,  96, 192)
    ]
};

function createStyle (fill_red, fill_green, fill_blue) {
    return new Style ({
        fill: new Fill ({
            color: 'rgba(' + fill_red + ', ' + fill_green + ', ' + fill_blue + ', ' + ALPHA + ')'
        }),
        stroke: new Stroke ({
            color: STROKE_COLOR,
            width: STROKE_WIDTH
        })
    });
}

function getStyle (has_candidate, style_id) {
    return STYLES[has_candidate][style_id];
}

/* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** ***
 * MAP
 */

const map_layer = new Tile ({
    source: new OSM()
});

const kml_layer = new Vector ({
    source: new VectorSource ({
        url: KML_PATH == null
            ? document.getElementById('kml_path').getAttribute('href')
            : KML_PATH,
        format: new KML ({
            extractStyles: false
        })
    }),
    style: function (feature) {
        return getStyle(hasCandidate(feature.get('name')), STYLE_NORMAL);
    }
});

const map = new Map ({
    layers: [map_layer, kml_layer],
    target: 'map',
    view: new View ({
        center: [14902340.23, -3216894.18],
        zoom: 3.5
    })
});

/* *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** ***
 * INTERACTIVITY
 */

var hovered = null;
var selected = null;

// HTML in `info` upon selecting a division.
function getInfoHTML (division_name, candidate_name, image_url, link_url) {
    if (candidate_name == null) {
        // Division has no candidate.
        return '<h2>Division of ' + division_name + '</h2>'
            + '<p>There is no candidate running in this electorate.</p>';
    }
    else {
        // Division has a candidate.
        return '<h2>Division of ' + division_name + '</h2><a href="' + link_url + '">'
            + '<img src="' + image_url + '" alt="' + candidate_name + '"></a><h3>'
            + candidate_name + '</h3>';
    }
}

function displayInfo (division_name) {
    if (hasCandidate(division_name)) {
        document.getElementById('info').innerHTML = getInfoHTML (
            division_name,
            getCandidateName(division_name),
            getImageUrl(division_name),
            getLinkUrl(division_name)
        );
    }
    else {
        document.getElementById('info').innerHTML = getInfoHTML(division_name);
    }
}

function setStyle (feature, style_id) {
    feature.setStyle(getStyle(hasCandidate(feature.get('name')), style_id));
}

function getFeatureAt (pos) {
    var features = map.getFeaturesAtPixel(pos);
    if (features != null && features.length > 0) {
        return features[0];
    }
    return null;
}

map.on('pointermove', function(evt) {
    if (evt.dragging) {
        return;
    }
    var feature = getFeatureAt(map.getEventPixel(evt.originalEvent));
    // Has the highlighted feature changed?
    if (feature == hovered) {
        // No, exit.
        return;
    }
    // Reset the style of the previous highlighted feature, if necessary.
    if (hovered != null && hovered != selected) {
        setStyle(hovered, STYLE_NORMAL);
    }
    // Assign the highlighted feature.
    hovered = feature;
    // Set the style of the current highlighted feature, if necessary.
    if (hovered != null && hovered != selected) {
        setStyle(feature, STYLE_HIGHLIGHT);
    }
});

map.on('click', function(evt) {
    var feature = getFeatureAt(evt.pixel);
    // Has the selected feature changed?
    if (feature == selected) {
        return;
    }
    // Reset the style of the previous selected feature, if necessary.
    if (selected != null) {
        setStyle(selected, STYLE_NORMAL);
    }
    // Assign the selected feature.
    selected = feature;
    // Set the style of the current selected feature if necessary, and display info.
    if (selected != null) {
        setStyle(selected, STYLE_SELECT);
        displayInfo(selected.get('name'));
    }
});