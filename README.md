# Setup

1. Clone the repository.
2. Navigate to the repository directory in a command-line interface.
3. Run `npm install`.
4. Run `npm start`.
5. In a web browser, open `localhost:1234`.

I have been experiencing what appears to be a bug where I sometimes need to run `npm start`, then
terminate the process (`^C`), then run it again for the webpage to load properly. Hopefully this
does not have implications beyond development.

Run `npm run build` to build the project to the `dist` directory.

# Files

- `index.html`: A minimum working example webpage.
- `index.js`: The source code.
- `package.json`: Node JS package file.
- `README.md`: This file.
- `data/candidates.json`: Specifies which electoral divisions have a candidate, the who the
candidates are, and URLs to their pictures and pages.
- `data/divisions.kml`: Contains geospatial data of electoral divisions. Adapted from
<http://www.tallyroom.com.au/maps>.

# Notes

## URL to the KML file

- The KML file is currently given as a `link` in `index.html` which is then obtained by
`index.js`. The reason for this is that Parcel renames files and rearranges directories without
refactoring strings containing file paths. Parcel **does** however, refactoring `href` attributes
in HTML files. Moreover, when I tried to simply using an absolute local path, I was unable to load
the webpage locally. This is because OpenLayers loads KML files using an `XMLHttpRequest`, and web
browsers refuse this for local files. Once the KML file is on a web server, replace the `KML_PATH`
variable in `index.js`. The `link` in `index.html` can then be removed.

## The HTML file

- `<link id="kml_path" href="data/divisions.kml">`: See note above.
- `<div id="map"></div>`: The `div` containing the map.
- `<div id="info"> … </div>`: The `div` where information about a selected electoral division is
displayed.
- `<script src="./index.js"></script>`: Loads `index.js`.

## The JS file

### Candidates

- `IMAGE_URL_PREFIX` and `LINK_URL_PREFIX` specify text to prepended to all URLs for candidate
images and links to candidate pages respectively, just to minimise the verbosity of the JSON file.
- `JSON_PATH` is the URL of `candidates.json`.
- See above regarding `KML_PATH`.

### Styles

- `ALPHA` is the alpha of all fill colours used by electoral divisions on the map.
- `STROKE_COLOR` and `STROKE_WIDTH` define the stroke used by electoral divisions on the map.
- The declaration of `STYLES` defines the fill colours of electoral divisions based on whether
they have a candidate and how the user is interacting with the map. Entries in `false` are styles
for divisions without a candidate, those in `true` for divisions with a candidate. The first style
is used when the user is not interacting with the division, the second when the user has their
pointer over the division, and the third when the user has selected the division.

### Interactivity

`getCandidateInfo(…)` defines what is displayed in `info` when a division is selected. The first
branch of the conditional is for divisions without a candidate, the second for those with a
candidate.

## The JSON file

Fairly self-explanatory. Recall that `image` and `link` are prepended with `IMAGE_URL_PREFIX` and
`LINK_URL_PREFIX`, defined in `index.js`.